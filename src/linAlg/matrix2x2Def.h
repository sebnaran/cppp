#ifndef MAT2x2_H
#define MAT2x2_H

#include "matrixDef.h"

namespace linAlg
{

//class Matrix;
//This is a specialized class for 2x2 matrices
class Matrix2x2: public Matrix
{
    public:
    Matrix2x2(){};
    /*! constructor
    \param a first  entry
    \param b second entry
    \param c third  entry
    \param d fourth entry*/
    Matrix2x2(const float a,const float b,const float c, const float d)
    {
        std::tuple<unsigned int,unsigned int> i00,i10,i01,i11;
        i00 = std::make_tuple(0,0);
        i10 = std::make_tuple(1,0);
        i01 = std::make_tuple(0,1);
        i11 = std::make_tuple(1,1);

        std::map<std::tuple<unsigned int,unsigned int>, float> entries {{i00, a},{i10,b},{i01,c},{i11,d} };
        makeMatrix(2,2,entries);
    };

    //This method returns the determinant of the matrix.
    float det()
    {
        float a = getEntry(0,0);
        float b = getEntry(1,0);
        float c = getEntry(0,1);
        float d = getEntry(1,1);
        return(a*d-b*c);
    }
    //method to print matrix
    void print()
    {
        //std::tuple<unsigned int,unsigned int> ij = std::make_tuple(0,0);
        //float a = _entries[ij];
        float a = getEntry(0,0);
        float b = getEntry(1,0);
        float c = getEntry(0,1);
        float d = getEntry(1,1);
        std::cout<<"|"<<a<<","<<b<<"|"<<std::endl;
        std::cout<<"|"<<c<<","<<d<<"|"<<std::endl;
    };

    private:

    protected:
};
}
#endif