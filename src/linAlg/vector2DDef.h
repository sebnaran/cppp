#ifndef VEC2x2_H
#define VEC2x2_H

#include "matrixDef.h"

namespace linAlg
{

//class Matrix;
//This is a specialized class for 2x2 matrices
class Vector2D: public Matrix
{
    public:
    Vector2D(){};
    /*! constructor
    \param a first entry
    \param b second entry*/
    Vector2D(const float a,const float b){
        std::tuple<unsigned int,unsigned int> i00,i10,i01,i11;
        i00 = std::make_tuple(0,0);
        i10 = std::make_tuple(1,0);

        std::map<std::tuple<unsigned int,unsigned int>, float> entries {{i00, a},{i10,b}};
        makeMatrix(2,1,entries);
    };

    /*! accessor to one of the entries
    \param i entry index*/
    float getEntry(const unsigned int i)
    {
        if(i>1)
            throw std::invalid_argument( "the input value must be 0 or 1" );
        else
        {
            std::tuple<unsigned int,unsigned int> ij = std::make_tuple(i,0);
            return(_entries[ij]);
        }    
    }

    //method to print vector
    void print() 
    {
        float a = getEntry(0);
        float b = getEntry(1);
        std::cout<<"|"<<a<<"|"<<std::endl;
        std::cout<<"|"<<b<<"|"<<std::endl;
    };

    private:

    protected:
};
}
#endif