#ifndef GENMAT_H
#define GENMAT_H

#include <iostream>
#include <map>
#include <tuple>

namespace linAlg
{
//This is the more general class of matrices. Meant to be used as a virtual class at least initially
class Matrix
{
    public:

    /*! This constructor turns a hash-map to a matrix
    \param row number of rows in the matrix
    \param col number of columns in the matrix
    \param entries a map to all the entries of the matrix*/
    Matrix(const unsigned int row, const unsigned int col,std::map<std::tuple<unsigned int,unsigned int>,float> entries): _row(row),_col(col),_entries(entries)
    {};

    Matrix(){};
    /*! 
    \param row number of rows in the matrix
    \param col number of columns in the matrix*/
    void makeMatrix(const unsigned int & row, const unsigned int & col,std::map<std::tuple<unsigned int,unsigned int>,float> & entries)
    {
        _row = row; _col = col; _entries = entries;
    }

    /*! accessor to one of the entries
    \param i first entry of the index
    \param j second entry of the index*/
    virtual float getEntry(const unsigned int i, const int j)
    {
        std::tuple<unsigned int,unsigned int> ij = std::make_tuple(i,j);
        return(_entries[ij]);
    }
    
    //Method to print matrix
    virtual void print()
    {
        throw std::invalid_argument("The print method has not been implemented for this type of matrix");
    }

    //This method computes the determinant of the matrix
    virtual float det()
    {
        if(_row ==_col)
            throw std::invalid_argument("The determinant method has not been implemented for general matrices.");
        else
            throw std::invalid_argument("The determinant can only be computed for square matrices.");
    };

    private:
    

    protected:
    std::map<std::tuple<unsigned int,unsigned int>,float> _entries; //a map to the entries of the matrix

    unsigned int _row,_col; //number of rows and columns
};

}

#endif