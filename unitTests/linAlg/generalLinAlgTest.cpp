#include <set>
#include <iostream>
#include <map>
#include <tuple>

#include <matrixDef.h>
#include <matrix2x2Def.h>
#include <vector2DDef.h>

int main()
{   
    linAlg::Matrix2x2 A(1.,2.,3.,4.);
    if (A.det() == -2.)
        return(0);
    else
        return(1);
}
