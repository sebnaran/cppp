This software is meant to be practice in development of tools for scientific computation.
The main goal is to develop a package that can find the mimima of a function given a pointwise evaluation using only the standard library.

The c++ tools we are using: hashmaps, tuples, inheritance, virtual methods, private, protected and public variables/methods, const tags, function overloading, namespaces, throwing error exceptions.

Development tools: git, cmake, unit testing, building libraries, linking libraries.
